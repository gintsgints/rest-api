package main_test

import (
	"os"
	"testing"

	"gitlab.com/gintsgints/rest-api/api/product"
	"gitlab.com/gintsgints/rest-api/app"
)

var a app.App

func TestMain(m *testing.M) {
	a = app.App{}
	a.Init()

	resetData()

	code := m.Run()

	// TODO: has to be implemented
	// clearData()

	os.Exit(code)
}

func resetData() {
	product := product.Product{}
	a.DB.Delete(&product)
}
