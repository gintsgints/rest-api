package api

import (
	"encoding/json"
	"net/http"
)

/*
Controller has helper functions for api
*/
type Controller struct {
}

/*
RespondWithError helper function for controller error response
*/
func (c Controller) RespondWithError(w http.ResponseWriter, code int, message string) {
	c.RespondWithJSON(w, code, map[string]string{"error": message})
}

/*
RespondWithJSON helper function for controller normal response
*/
func (c Controller) RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
