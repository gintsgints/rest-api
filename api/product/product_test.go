package product_test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"gitlab.com/gintsgints/rest-api/api/product"
	"gitlab.com/gintsgints/rest-api/app"
)

var firstProdID uint

var a = app.App{}

func TestMain(m *testing.M) {
	a.Init()
	os.Exit(m.Run())
}

// product_test.go
func TestProductPost(t *testing.T) {
	p := product.Product{Code: "First code", Price: 10}
	strB, err := json.Marshal(p)
	if err != nil {
		t.Errorf("POST /products: Failed to parse Product type, Error: %s", err)
	}

	req := httptest.NewRequest("POST", "/products", strings.NewReader(string(strB)))
	w := httptest.NewRecorder()
	a.Router.ServeHTTP(w, req)
	if w.Code != 201 {
		t.Errorf("POST /products: got code %d, want 201", w.Code)
	}
	decoder := json.NewDecoder(w.Body)
	var res product.Product
	if err := decoder.Decode(&res); err != nil {
		t.Errorf("POST /products: failed to parse response. %s, Error: %s", w.Body.String(), err)
	}
	firstProdID = res.ID
	if code := res.Code; strings.Compare(code, "First code") != 0 {
		t.Errorf("POST /products: expected response Code to be 'First Code' in body, got %s", code)
	}
}
func TestProductSecondPost(t *testing.T) {
	p := product.Product{Code: "Second code", Price: 20}
	strB, err := json.Marshal(p)
	if err != nil {
		t.Errorf("POST /products: Failed to parse Product type, Error: %s", err)
	}

	req := httptest.NewRequest("POST", "/products", strings.NewReader(string(strB)))
	w := httptest.NewRecorder()
	a.Router.ServeHTTP(w, req)
	if w.Code != 201 {
		t.Errorf("POST /products: got code %d, want 201", w.Code)
	}
	decoder := json.NewDecoder(w.Body)
	var res product.Product
	if err := decoder.Decode(&res); err != nil {
		t.Errorf("POST /products: failed to parse response. %s, Error: %s", w.Body.String(), err)
	}
	if code := res.Code; strings.Compare(code, "Second code") != 0 {
		t.Errorf("POST /products: expected response Code to be 'Second code' in body, got %s", code)
	}
}

func TestProductList(t *testing.T) {
	req := httptest.NewRequest("GET", "/products", nil)
	w := httptest.NewRecorder()
	a.Router.ServeHTTP(w, req)
	if w.Code != 200 {
		t.Errorf("GET /products: got code %d, want 200", w.Code)
	}
	decoder := json.NewDecoder(w.Body)
	var p []product.Product
	if err := decoder.Decode(&p); err != nil {
		t.Errorf("GET /products: failed to parse response. %s, Error: %s", w.Body.String(), err)
	}
	if i := len(p); i != 2 {
		t.Errorf("GET /products: should return two products but got %d", len(p))
	}
	if code := p[0].Code; strings.Compare(code, "First code") != 0 {
		t.Errorf("GET /products: expected 'First Code' in body, got %s", code)
	}
}

func TestProductGet(t *testing.T) {
	req := httptest.NewRequest("GET", "/products/"+fmt.Sprint(firstProdID), nil)
	w := httptest.NewRecorder()
	a.Router.ServeHTTP(w, req)
	if w.Code != 200 {
		t.Errorf("GET /products: got code %d, want 200", w.Code)
	}
	decoder := json.NewDecoder(w.Body)
	var p product.Product
	if err := decoder.Decode(&p); err != nil {
		t.Errorf("GET /products: failed to parse response. %s, Error: %s", w.Body.String(), err)
	}
	if code := p.Code; strings.Compare(code, "First code") != 0 {
		t.Errorf("GET /products: expected 'First Code' in body, got %s", code)
	}
}

func TestProductPut(t *testing.T) {
	p := product.Product{Code: "Updated Code"}
	strB, err := json.Marshal(p)
	if err != nil {
		t.Errorf("PUT /products: Failed to parse Product type, Error: %s", err)
	}

	req := httptest.NewRequest("PUT", "/products/"+fmt.Sprint(firstProdID), strings.NewReader(string(strB)))
	w := httptest.NewRecorder()
	a.Router.ServeHTTP(w, req)
	if w.Code != 200 {
		t.Errorf("PUT /products: got code %d, want 200", w.Code)
	}
	decoder := json.NewDecoder(w.Body)
	var res product.Product
	if err := decoder.Decode(&res); err != nil {
		t.Errorf("POST /products: failed to parse response. %s, Error: %s", w.Body.String(), err)
	}
	if err := decoder.Decode(&res); err != nil {
		t.Errorf("POST /products: failed to parse response. %s, Error: %s", w.Body.String(), err)
	}
	if code := res.Code; strings.Compare(code, "Updated Code") != 0 {
		t.Errorf("POST /products: expected response Code to be 'Updated Code' in body, got %s", code)
	}
	if price := res.Price; price != 10 {
		t.Errorf("POST /products: expected response Code to be 10 in body, got %v", price)
	}
}

func TestProductDelete(t *testing.T) {
	req := httptest.NewRequest("DELETE", "/products/"+fmt.Sprint(firstProdID), nil)
	w := httptest.NewRecorder()
	a.Router.ServeHTTP(w, req)
	if w.Code != 200 {
		t.Errorf("DELETE /products: got code %d, want 200", w.Code)
	}

	req = httptest.NewRequest("GET", "/products/"+fmt.Sprint(firstProdID), nil)
	w = httptest.NewRecorder()
	a.Router.ServeHTTP(w, req)
	if w.Code != 404 {
		t.Errorf("GET /products: after delete got code %d, want 404", w.Code)
	}
}

func BenchmarkProductList(b *testing.B) {
	s := httptest.NewServer(a.Router)
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		res, err := http.Get(s.URL + "/products")
		if err != nil {
			b.Fatal(err)
		}
		if res.StatusCode != 200 {
			b.Fatalf("GET /: expected code 200, got %d", res.StatusCode)
		}
		n, err := ioutil.ReadAll(res.Body)
		if err != nil {
			b.Fatal(err)
		}
		b.SetBytes(int64(len(n)))
		res.Body.Close()
	}
}
