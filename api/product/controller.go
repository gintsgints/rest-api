package product

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/gintsgints/rest-api/api"
)

/*
Ctrl test
*/
type Ctrl struct {
	api.Controller
}

/*
Index GET list of products
*/
func (c Ctrl) Index(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	var products []Product
	db.Find(&products)
	c.RespondWithJSON(w, http.StatusOK, products)
}

/*
Post POST to insert product
*/
func (c Ctrl) Post(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	var p Product
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		c.RespondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer r.Body.Close()

	if err := db.Create(&p).Error; err != nil {
		c.RespondWithError(w, http.StatusInternalServerError, "Failed to save object to DB")
		return
	}

	c.RespondWithJSON(w, http.StatusCreated, p)
}

/*
Get GET product
*/
func (c Ctrl) Get(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	params := mux.Vars(r)
	product := Product{}
	if db.First(&product, params["id"]).Error != nil {
		c.RespondWithError(w, http.StatusNotFound, "Object not found")
	} else {
		c.RespondWithJSON(w, http.StatusOK, product)
	}
}

/*
Put PUT to patch product
*/
func (c Ctrl) Put(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	params := mux.Vars(r)
	product := Product{}
	if db.First(&product, params["id"]).Error != nil {
		c.RespondWithError(w, http.StatusNotFound, "Object not found")
	} else {
		c.RespondWithJSON(w, http.StatusOK, product)
	}

	updated := Product{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&updated); err != nil {
		c.RespondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	defer r.Body.Close()

	db.Model(&product).Updates(&updated)

	// TODO: find out why response sends two objects. original & updated
	c.RespondWithJSON(w, http.StatusOK, product)
}

/*
Del to delete product
*/
func (c Ctrl) Del(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	params := mux.Vars(r)
	product := Product{}
	if db.First(&product, params["id"]).Error != nil {
		c.RespondWithError(w, http.StatusNotFound, "Object not found")
	} else {
		if db.Delete(&product).Error != nil {
			c.RespondWithError(w, http.StatusInternalServerError, "Failed to delete")
		} else {
			c.RespondWithJSON(w, http.StatusOK, product)
		}
	}
}
