package product

import (
	"github.com/jinzhu/gorm"
)

/*
Product model
*/
type Product struct {
	gorm.Model
	Code  string `json:"code"`
	Price uint   `json:"price"`
}

/*
Products - Slice of all Products
*/
type Products []Product
