package app

import (
	"bytes"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/gintsgints/rest-api/api/product"
)

/*
App REST application
*/
type App struct {
	Router *mux.Router
	DB     *gorm.DB
}

/*
Init - Initializes Database
*/
func (app *App) Init() {
	if err := LoadConfig(); err != nil {
		panic(fmt.Errorf("Invalid application configuration: %s", err))
	}

	var buffer bytes.Buffer
	buffer.WriteString(Config.User)
	buffer.WriteString(":")
	buffer.WriteString(Config.Pass)
	buffer.WriteString("@tcp(")
	buffer.WriteString(Config.DbHost)
	buffer.WriteString(":3306)/")
	buffer.WriteString(Config.Db)
	buffer.WriteString("?charset=utf8&parseTime=True&loc=Local")

	// Database initialization
	var err error
	app.DB, err = gorm.Open(Config.Driver, buffer.String())
	if err != nil {
		panic(fmt.Sprintf("failed to connect database %v", err))
	}
	app.DB.AutoMigrate(&product.Product{})

	app.Router = NewRouter(app.DB)
}

/*
Run - start webserver
*/
func (app *App) Run() {
	log.Fatal(http.ListenAndServe(":8080", app.Router))
}
