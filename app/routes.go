package app

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/gintsgints/rest-api/api/product"
)

type service interface {
	Index(w http.ResponseWriter, r *http.Request, db *gorm.DB)
	Post(w http.ResponseWriter, r *http.Request, db *gorm.DB)
	Get(w http.ResponseWriter, r *http.Request, db *gorm.DB)
	Put(w http.ResponseWriter, r *http.Request, db *gorm.DB)
	Del(w http.ResponseWriter, r *http.Request, db *gorm.DB)
}

func mesure(service) {

}

/*
NewRouter Initializes all app routes.
*/
func NewRouter(db *gorm.DB) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	addRoute("/products", router, product.Ctrl{}, db)
	router.Use(Logger)
	return router
}

func addRoute(path string, r *mux.Router, s service, db *gorm.DB) {
	list := r.Path(path).Subrouter()
	list.Methods("GET").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s.Index(w, r, db)
	})
	list.Methods("POST").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s.Post(w, r, db)
	})
	element := r.Path("/products/{id}").Subrouter()
	element.Methods("GET").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s.Get(w, r, db)
	})
	element.Methods("PUT").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s.Put(w, r, db)
	})
	element.Methods("DELETE").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s.Del(w, r, db)
	})
}
