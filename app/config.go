package app

import (
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/tkanos/gonfig"
)

type appConfig struct {
	Driver string `mapstructure:"driver"`
	User   string `mapstructure:"user"`
	Pass   string `mapstructure:"pass"`
	Db     string `mapstructure:"db"`
	DbHost string `mapstructure:"dbhost"`
}

// Config stores the application-wide configurations
var Config appConfig

/*
LoadConfig - loads application configuration
*/
func LoadConfig() error {
	Config = appConfig{}
	return gonfig.GetConf(getFileName(), &Config)
}

func getFileName() string {
	env := os.Getenv("GO_ENV")
	if len(env) == 0 {
		env = "development"
	}
	filename := []string{env, ".json"}
	_, dirname, _, _ := runtime.Caller(0)
	filePath := path.Join(filepath.Dir(dirname), "config", strings.Join(filename, ""))

	return filePath
}
