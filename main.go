package main

/*
Rest API example

Thank you:
https://semaphoreci.com/community/tutorials/building-and-testing-a-rest-api-in-go-with-gorilla-mux-and-postgresql
https://github.com/qiangxue/golang-restful-starter-kit
https://github.com/corylanou/tns-restful-json-api

Next to implement - https://github.com/ant0ine/go-json-rest#gorm
*/

import "gitlab.com/gintsgints/rest-api/app"

func main() {
	a := app.App{}
	a.Init()
	a.Run()
}
