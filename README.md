# golang REST api example 

Project is created to make such features to work:

* Easy creation of development environmet
* Make development cycle short (autocompile, autodeploy)
* Clear layout - all endpoint code should be at one place
* ORM usage for model definitions and database migrations
* Perfomance - API should still be super fast
* Scalability - API should be able to scale inside machine and against multiple machines

## Usage

To build project execute

```
go get -u github.com/golang/dep/cmd/dep
dep ensure -update
go build *.go
```

To start api with docker execute:

```
export GO_ENV=development
docker-compose build
docker-compose up -d
```

If you want to start with existing local database server do:

```
export GO_ENV=local
go get github.com/markbates/refresh
refresh run
```

To start tests execute:

```
docker-compose run app go test -v ./...
```

