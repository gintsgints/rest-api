FROM golang:1.9.2 as build-go

ENV D=/go/src/gitlab.com/gintsgints/rest-api

RUN apt-get update && apt-get install -y netcat \ 
    mysql-client \
    && rm -rf /var/lib/apt/lists/*

RUN go get -u github.com/golang/dep/...
RUN go get github.com/markbates/refresh
ADD . $D
RUN cd $D && dep ensure -v --vendor-only
RUN cd $D && go build -o main && cp main /tmp/

FROM alpine
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
RUN apk --no-cache add ca-certificates
WORKDIR /app/server/
COPY --from=build-go /tmp/main /app/server/
EXPOSE 8080
CMD ["./main"]
